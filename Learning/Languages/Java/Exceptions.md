# Exceptions

```java
try{
    // Programmcode der eine Exception ausloesen kann
}catch(Exception e){
    // Programmcode zum Behandeln der Exception
}finally{
    // Programmcode der immer am Ende ausgefuehrt wird
}

try{...}
catch(IOException | FileNotFoundException){...}

throw new IllegalArgumentException("Alter < 18");

try(Scanner res = new Scanner(in)){...} // try with Ressources : autocloseable
```
