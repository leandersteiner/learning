# Generics

```java
public class Rocket<T> {

    private T value;

    public Rocket(T value){
        this.value = value;
    }

    public void set(T value){
        this.value = value;
    }

    public T get(){
        return value;
    }
}

Rocket<Integer> intRocket = new Rocket<Integer>(7);
Rocket<String> stringRocket = new Rocket<String>("Hi");
Rocket<Rocket<String>> rocketOfRockets = new Rocket<Rocket<String>>();
```

## Generic Interfaces

```java
public interface Comparable<T> {
    int compareTo(T o);
}

public interface Set<E> extends Collection<E> {
    boolean add(E e);
    Iterator<E> iterator();
    ...
}
```

## Generic Methods

```java
public class GenericMethods {
    public static <T> T random(T m, T n){
        return Math.random() > 0.5 ? m : n;
    }
}

String s = random("Option 1", "Option 2");
```

**Basic Usage:** ```modifier <type> returnType methodName(Parameters);```

## Einschraenkungen ueber Bounds

```java
public static <T extends CharSequence> T random(T n, T m){
    return Math.random() > 0.5 ? m : n;
}
```

Nur Klassen die CharSequence erweitern sind erlaubt. Ohne Type-Bound koennten nure die Methoden Object genutzt werden. Auch mit ```implements``` fuer interfaces moeglich

```java
<T extends Class1 & Interface1 & Interface2 ...>
```

Mit dem Wildcard-Typ ```?``` kann man einen unbekannten Typ angeben:

```java
public static boolean isOneRocketEmpty(Rocket<?> ... rockets){
    for(Rocket<?> rocket : rockets){
        if(rocket.isEmpty()){
            return true;
        }
    }
}
```