# Packages

Wenn kein Package angeben wurde, steht es im daefault package. was problematisch ist, da es nicht von unter-paketen genutzt werden kann. Auch ist es problematisch wenn wir mit Packages von anderen Autoren arbeiten wollen. Daher sollten wir immer ein package definieren:
```java
package de.leanstein.packagename;

import de.leanstein.packagename.klasse;
```
Die Struktur ist dabei lediglich eine Verzeichnisstruktur, die unser Package repräsentiert.
Hier wäre es zum Beispiel: ```de\leanstein\packagename\klasse.java```.

Bei Klassen mit mit gleichem Namen in unterschieldichen Packages ist ein voller Qualifiziere nötig also wäre ```de.leanstein.package.*``` nicht möglich wenn wir auch ```de.package.new.*``` importieren und beide Packages eine Klasse mit dem Namen ```Test``` haben und wir beide benutzen wollen.

Pro Klasse kann es nur eine Package-Deklaration geben.

### Statische Imports (sollten nicht unbedingt verwendet werden)
Wir könne auch so Packages einbinden:
```java
import static java.lang.System.out;

out.println("Hello"); // legal
```

Mit einem statischen Import werden dem Compiler die statischen Methoden einer eingebundenen Klasse bekannt gemacht, sodass man sie auch ohne den vollen Qualifiziere nutzen kann.

