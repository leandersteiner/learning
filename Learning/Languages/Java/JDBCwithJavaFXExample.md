# Little Example for JDBC with JavaFX
Just a little test where I wanted to see if I have everything set up correctly. To use ```mysql``` with ```JDBC``` you have to download the driver and include it in your classpath.
```java
package app;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import java.sql.*;

public class Main extends Application{
    public static void main(String[] args) throws SQLException {
         launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        VBox root = new VBox(5.0);
        GridPane inputs = new GridPane();
        inputs.setVgap(5.0);

        TextField username = new TextField();
        PasswordField password = new PasswordField();
        Button login = new Button("Login");

        login.setOnAction(event -> {
            String usernameText = username.getText();
            String passwordText = password.getText();
            int id = 0;
            try{
                Connection con = DriverManager.getConnection("jdbc:mysql://localhost/app", "root", "");
                Statement s = con.createStatement();
                String sql = "SELECT * FROM user";
                ResultSet rs = s.executeQuery(sql);

                while(rs.next()){
                    if(rs.getString(2).equals(usernameText) && rs.getString(3).equals(passwordText)){
                        id = rs.getInt(1);
                        break;
                    }
                }

                con.close();
                System.out.println("ID: " + id);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        inputs.getChildren().addAll(username, password);
        GridPane.setConstraints(username, 1, 1);
        GridPane.setConstraints(password, 1, 2);

        root.getChildren().addAll(inputs, login);

        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Datenbank-Test");
        primaryStage.show();
    }
}

```