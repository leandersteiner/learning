# Zeichen und Zeichenketten

## Character
Die Klasse ```Character``` bietet und einige Methoden für die Arbeit mit ```char```. Zum Beispiel die methoden ```isXXXX(char)```.
Mit diesen können wir bestimmte Eigenschaften von Zeichen abfragen.
**Beispiele:**
```java
Character.isDigit('0'); // true
Character.isLetter('a'); // true
Character.isWhitespace(' '); // true
Character.isLetterOrDigit('a');
Character.isLowerCase('a');
```

**Konvertierungen:**
```java
Character.toUppercase(c);
Character.toLowerCase(c);
String.valueOf(c);
Character.toString(c);

char c = '5';
int i = Integer.parseInt(String-valueOf(c));

int j = Character.getNumericValue('5'):
```

## Strings & StringBuilder
```java
int num = 22;
String s = "Anzahl: " + num;

System.out.println(s);
System.out.println(s.legth()); // length ist hier eine Methode
System.out.println(s.isEmpty());
System.out.println(s.isBlank());

System.out.println(s.charAt(2)); // z
System.out.println(s.contains("zahl")); // true

System.out.println(s.indexOf('z')); // 2
System.out.println(s.lastIndexOf('z')); // 2

```

```java
String s1 = "Hallo Welt!";
String s2 = "hallo welt!";

System.out.println(s1.equals(s2)); // false
System.out.println(s1.equalsIgnoreCase(s2)); // true

System.out.println(s1.startsWith("Hallo")); // true
System.out.println(s2.endsWith("z")); // false
```
Für Patternmatching gibt es die ```.matches()``` Methode und die Klassen ```Pattern``` und ```Matcher```.

```java
String s1 = "Willkommen im Club";
String s2 = s1.substring(0, 10);
System.out.println(s2); // Willkommen
```

```java
String s = String.join("-", "Hallo", "Welt", "wie", "gehts", "?");
System.out.println(s);
```

### Whitespace entfernen
```java
String s = "    Hallo Welt wie geht es ? ";
s.trim(s);
s.strip(s);
s.stripLeading(s);
s.stripTrailing(s);
```

### Ersetzen
```java
String s = "lalalala";
System.out.println(s.replace('a', 'o')): // lolololo
```

## StringBuilder
StringBuilder verhält sich bei Methoden wie ```substring()``` genauso wie ein normales String-Objekt. 
```java
StringBuilder sB = new StringBuilder("Hallo Welt");
String s = sB.troString();
sB.append(" na wie gehts?");
sB.append(System.lineSeparator());
sB.append("Zweite Zeile");
sB.setCharAt(5, 's');
sB.insert(5, "Hallo");
sB.delete(5, 12);
sB.replace(5, 12, "Neuer String");
System.out.println(sB.length());
System.out.println(sB.capacity());
```

**Der Basistyp jeglicher Zeichenketten ist ```CharSequence```.**
```java
"Hallo Welt".chars().forEach(c -> { // umwandeln in einen Stream
    System.out.println((char)c);
});
```

## Stringdarstellung Hex, Bin, etc.
```java
System.out.println(Integer.toHexString(15));
System.out.println(Integer.toBinaryString(15));
System.out.println(Integer.toOctalString(15)); 
```
Auch für Long etc. verfügbar.

**Nützliche Klassen: ```DatatypeConverter StringJoiner StringBuffer Scanner StringTokenizer BreakIterator Matcher```**

```java
String toSplit = "www.webseite.de";
String[] parts = toSplit.split(Pattern.quote("."));
System.out.println(Arrays.toString(parts)); // [www, webseite, de]
```