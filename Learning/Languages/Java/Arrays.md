# Arrays
Simpler Datentyp zum Speichern von mehreren Werten des gleichen Typs.
Es können sowohl primitve Datentypen als auch Refernztypen und weitere Arrays gespeichert werden.
```java
int[] nums; // Deklaration
nums = new int[2]; // Initialisierung
int[] nums2 = new int[2];

nums[0] = 2; // Erstes Element setzen
nums[3] = 4; // Letztes Element setzen

int[] nums3 = {2, 0, 4}; // Alle Elemente setzen

System.out.println(nums3[0]); // 2
System.out.println(nums3.get(0)); // 2
System.out.println(nums3.length); // 3 Länge des Arrays (final)

// Jedes Element eines Arrays ausgeben
for(int num : nums3){
    System.out.println(num);
}
```
### Methoden mit variabler Argumentanzahl
```java
public void names(String ... array){
    for(String name : array){
        System.out.println(name);
    }
}
```
Alle übergebenen Werte, werden der Funktion als Array zur Verfügung gestellt.

```java
public void names2(String name, String ... names){
    // der erste String ist in name verfügbar, der Rest in names
}
```

### Mehrdimensionale Arrays
Sind einfach Arrays die in anderen Arrays stehen.
Zweifach: 2D Darstellung von z.B Spielfeldern
Dreifach: 3D Darstellung z.B Vectoren für 3D Spiele
```java
int[][] spielfeld = new int[4][4]; // 4x4 Spielfeld

// Nach aktiven Feldern suchen. Aktiv wenn 1 Inaktiv wenn 0
for(int row = 0; row < spielfeld.length; row++){
    for(int col = 0; col < spielfed[row].lentgh; col++){
        if(spielfeld[row][col] == 1){
            System.out.println("Aktives Feld bei" + row + "|" col);
        }
    }
}

int[][] spielfeld2 = 
{
    {1, 2, 3}, 
    {1, 2, 3}, 
    {1, 2, 3}
}; // 3x3 Array
```

Mehrdimensionale Arrays müssen nicht Sysmetrisch sein.

```java
int[][] spielfeld3 = 
{
    {1, 2, 3, 4, 5, 6}, 
    {1, 2, 3}, 
    {1, 2, 3, 4, 5}
}; // 3x3 Array
```

### Arrays klonen
```java
int[] nums = {1, 2, 3};
int[] nums2 = nums.clone();
```

### Die Klasse *Arrays*
```import java.util.Arrays;```
```java
String[] names = {"Leander", "Erik", "Niels"};
System.out.println(names); // Nicht das gewünschte Ergebnis [java.lang.String...
System.out.println(Arrays.toString(names)); // [Leander, Erik, Niels]

Arrays.sort(names);
Arrays.parallelSort(names); // Multithreaded

int[][] a1 = {{0, 1},{1, 0}};
int[][] a2 = {{0, 1},{1, 0}};
System.out.println(Arrays.equals(a1, a2)); // false
System.out.println(Arrays.deepEquals(a1, a2)); // true
```
```deepEquals()``` bezieht auch Arrays in Arrays mit in den Vergleich ein.
Arrays bietet noch viele weitere statische Methoden die nützlich sind. Einfach mal in die [Docs](https://docs.oracle.com/javase/8/docs/api/java/util/Arrays.html) schauen.