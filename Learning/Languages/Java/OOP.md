# OOP

### Eigenschaften von Objekten
- Jedes Objekt hat eine Identität
- Jedes Objekt hat einen Zustand
- Jedes Objekt hat ein Verhalten

**Eine Instanz einer Klasse mit dem *new* Keyword erstellen:**

```java
Scanner sc = new Scanner(System.in); // Neues Objekt vom typ Scanner instanziiert
```
Den Speicherplatz für unser neues Objekt nimmt die Laufzeitumgebung vom Heap.
Der Heap wächst von einer Startgröße bis zu einer erlaubten Maximalgröße.
Wird das Objekt nicht mehr vom Programm referenziert, so wird der Speicher vom Garbage-Collector wieder freigegeben.
Ein Verweis auf ein Objekt ist intern ein Pointer auf einen Speicherbereich.

**Zugriff auf Objektmethoden:**

```java
Scanner sc = new Scanner(System.in);
int age = sc.nextInt();
```
## Referenzen

### null-Referenzen

- **note:** *in "Modern Java in Action" wird von "Optional" als bessere alternative zu null gesprochen*

- Drei wichtige Referenzen: this, super & null
- Mit der null-Referenz können wir Referenzvariablen initialisieren.
- Kann jeder Methode übergeben werden die ein Objekt erwartet.
```java
Point p = null;
String s = null;
System.out.println(p); // null
```
#### Haupteinsatzzwecke:

- uninitialisierte Referenzvariablen kennzeichnen
- in Datenstrukturen: kein gültiger Nachfolger vorhanden
- Referenzen testen

Wenn wir versuchen auf Eigenschaften eines Objekts mit dem Typ ```null``` zuzugreifen, gibt es eine ```NullPointerException```.

```java
if(object == null){
    // Objekt ist nicht Initialisiert
}else{
    // Können mit dem Objekt arbeiten
}
```
```java
// Bei Eingaben
import javafx.scene.control.TextInputDialog;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application{
    public static void main(String[] args){
        launch(args);
    }

    @Override
    public void start(Stage primaryStage){
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Title");
        dialog.setHeaderText("Eingabe";
        dialog.setContentText("Bitte geben sie ihren Namen ein.");

        String result = dialog.showAndWait().get(); // Gibt nur mit .get() einen String zurück

        if(result != null && !result.isEmpty()){
            System.out.println("Name: " + result);
        }else{
            System.out.println("Bitte füllen sie den Dialog aus.");
        }
    }
}
```

### Alias

Mehrere Referenzvariablen speichern die gleiche Referenz.
```java
import java.awt.Point;
```
```java
Point p = new P();
Point q = p;
p.x = 10;
System.out.println(q.x); // 10
q.y = 5;
System.out.println(p.y); // 5
```

### Gleichheit

Mit Objekten kann man den ```==``` Vergleichsoperator nicht zuverlässig nutzen.
Hierfür gibt es die ```obj1.equals(obj2)``` Methode.
```java
Point p = new Point(10, 10);
Point q = new Point(10, 10);
System.out.println(p == q); // False da jedes Objekt eigene Identität
System.out.println(p.equals(q)); // True
```
Jedes Object verfügt über die ```.equals(Object o)``` Methode, da diese automatisch von der Oberklasse ```Object```, von der jede Klasse in Java automatisch erbt, geerbt wird.
