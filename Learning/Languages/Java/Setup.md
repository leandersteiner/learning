# Setup
To get started with Java you will need to install a version of the Java JDK.

For Windows just download the OpenJDK binaries and extract them somewhere and then create a PATH variable to it.

On Linux, install sdkman to manage your java installations.

Once ready we can use javac to compile and java to start the compiled program, jshell will be available too which is a REPL for java.

After that we need to decide on an editor:

- IDE like IntelliJ, Netbeans, Eclipse
- Editors like notepad++, vim, vs code

You should start with an editor not an IDE and compile using the command line

Try ideas immediately, always think about corner cases and different possibilities to use what you learned or how to use other techniques for a problem

## Hello World Example

```java
public class HelloWorld {
	public static void main (String[] args) {
		System.out.println("Hello World!");
	}
}
```
```bash
$ javac HelloWorld.java
$ java HelloWorld
Hello World!
    
$ javac HelloWorld.java && java HelloWorld
Hello World!
    
jshell> System.out.println("Hello World!");
Hello World!
```