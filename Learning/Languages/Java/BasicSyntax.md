# Basic Syntax

- Java kodiert Texte durch Unicode-Zeichen
- Es werden Bezeichner zur Bennenung von Variablen, Konstanten, Methoden, Klassen und Interfaces benutzt

## Klassendeklaration

Deklariert eine neue Klasse mit dem Namen "Main" (Datei muss wie Klasse heißen hier **Main.java**)
```java
public class Main{
    public static void main(String[] args) {
        // Einstiegspunkt
    }
}
```
Klassen sind Baupläne für Objekte, welche die kleinste Einheit in einem Objektorientiertem System darstellen.
Methoden sind Funktionen innerhalb eines Objekts und Attribute sind Variablen innerhalb eines Objekts.
Jedes Objekt hat einen Zustand(Attribute), ein Verhalten(Methoden) und eine Identität. Zustand und Verhalten werden so zu einer Einheit gekapselt.
Das Verhalten ändert den internen Zustand eines Objekts, ein Zugriff von außen ist nur über die öffentlichen Schnittstellen des Objekts möglich(Messaging).

## Ausgabe

Methoden aufruf, der Text auf der Konsole ausgibt.

```java
System.out.println("Ausgabe");
System.out.println(21.6);
System.out.println(true);
```
Die Funktion *println* ist kann mit allen möglichen Datentypen aufgerufen werden, da es sich um eine überladene Methode handelt.

## Hello World

```java
public class Main{
    public static void main(String[] args) {
        System.out.println("Hello World");
    }
}
```
Java führt alle Anweisungen sequentiell aus.

## Datentypen

Java nutzt Variablen zum ablegen von Daten.
Variablen sind Speicherbereiche die von uns mit einem Bezeichner angesprochen werden können. Die größe des Bereichs hängt von der größe des Datentyps ab.
Es gibt primitive Datentypen und Referenztypen. Erstere sind die bereits in Java vorhandenen Datentypen, letztere sind Objektverweise auf von uns erstellte Objekte.

Typ|Bedeutung
-|-
boolean | ```true``` oder ```false```
char | 2 Byte Unicode-Zeichen
byte | 1 Byte Ganzzahl
short | 2 Byte Ganzzahl
int | 4 Byte Ganzzahl
long | 8 Byte Ganzzahl
float | 4 Byte Fließkommazahl
double | 8 Byte Fließkommazahl

Strings gehören nicht zu den primitiven Datentypen.

```java
System.out.println(Character.MIN_VALUE);
System.out.println(Character.MAX_VALUE);
System.out.println(Byte.MIN_VALUE);
System.out.println(Byte.MAX_VALUE);
System.out.println(Short.MIN_VALUE);
System.out.println(Short.MAX_VALUE);
System.out.println(Integer.MIN_VALUE);
System.out.println(Integer.MAX_VALUE);
System.out.println(Long.MIN_VALUE);
System.out.println(Long.MAX_VALUE);
System.out.println(Float.MIN_VALUE);
System.out.println(Float.MAX_VALUE);
System.out.println(Double.MIN_VALUE);
System.out.println(Double.MAX_VALUE);
```

## Variablendeklaration

```java
String name; // Deklaration
int count;
double money;
char gender;
boolean isDone;

name = "Leander Steiner"; // Initialisierung
```
Der Datentyp wird immer vor den Bezeichner geschrieben und ist zwingend notwendig.
Man kann auch direkt bei der Deklaration der Variable diese mit einem Wert initialisieren.
```java
String name = "Leander Steiner"; // Deklaration und Initialisierung
char gender = 'm';
```
Wir können es auch dem Compiler überlassen, den richtigen Datentyp auszuwählen:
```java
var name = "Leander Steiner";
var gender = 'm';
```
Hier muss immer direkt der Wert initialisiert werden.

## Werte von der Konsole einlesen

Mit der Klasse ```java.util.Scanner``` können wir ziemlich einfach Werte einlesen.
```java
String name = new java.utils.Scanner(System.in).nextLine();
int age = new java.utils.Scanner(System.in).nextInt();
```
Wir können aber auch die Scanner-Klasse importieren und es somit etwas übersichtlicher gestalten:
```java
import java.util.Scanner;
public class Main{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        int age = sanner.nextInt();
        System.out.println(name + " " + age);
    }
}
```

## Operatoren (ohne Bitweiseoperatoren)

Operator|Funktion
:-:|-
=|Zuweisung
+|Addition
-|Subtraktion
*|Multiplikation
/| Division
%|Modulo - Rest einer Division
++|Inkrement Präfix oder Postfix
--|Dekrement Präfix oder Postfix
\>|größer
<|kleiner
\>=|größer/gleich
<=|kleiner/gleich
==|Test auf Gleichheit
!=|Test auf Ungleichheit
!|Negation
&&|logisches Und
\|\||logisches Oder

## Kontrollstrukturen

**Verzweigungen mit if:**
```java
int age = 22;
if(age < 18){
    System.out.println("Der Film ist ab 18, sorry!");
}else if(age >= 16){
    System.out.println("Wenn du ein Elternteil mitbringst darfste rein.");
}else{
    System.out.println("Bitte hier entlang!");
}
```
Mit ```if``` können wir Konditionen prüfen und jenachdem ob diese den Wert ```true``` oder ```false``` liefern, Entscheidungen treffen.

**Fallunterscheidung mit Switch:**
```java
switch(buchstabe){
    case 'a':
        System.out.println("Der Buchstabe ist a");
        break;
    case 'b':
        System.out.println("Der Buchstabe ist b");
        break;
    default:
        System.out.println("Der Buchstabe ist weder a noch b");
}
```
**Schleifen mit while:**
```java
while(true){ // Wiederholung bis Bedinung false ist
    System.out.println("Endlosschleife");
}
```

**Schleifen mit for:**
```java
for(int i = 0; i < 10; i++){
    System.out.println(i);
}

// Endlosschleife
for(;;){
    System.out.println("Endlosschleife");
}
```

**For-Each-Schleife:**
```java
//for-each Schleife
String[] names = {"Leander", "Niels", "Erik"};
for(String name : names){
    System.out.println(name);
}
```
