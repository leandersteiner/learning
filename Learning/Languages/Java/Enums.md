# Enums

```java
public enum Weekday {
    MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY
}

Weekday.MONDAY.toString();
Weekday.MONDAY.name();
Weekday.valueOf("MONDAY").name();

for(Weekday day : Weekday.values()){
    System.out.println(day.name());
}
```