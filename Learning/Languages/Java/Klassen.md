# Klassen

## Bestandteile

- Attribute (Zustand)
- Methoden (Verhalten)
- Konstruktoren
- geschachtelete Klassen, Schnittstellen, Aufzahlungen

## Klassendklaration

```java
class ClassName {} // without public, package private

class Player {
    String name;
    int level;

    void increaseLevel(int levels){
        level += levels;
    }
    boolean completedFiveLevels(){
        level > 5 ? return true : return false;
    }
}

class Game {
    Player player = new Player();
    player.name = "Name";
    player.level = 1;
}
```

## Sichtbarkeiten

- public
- private
- protected

## Konstanten

```java
public static final int PI = 3.14;
```

## Aufzaehlungstypen

```java
public enum Days {
    Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday
}
```

## Konstruktoren

```java
class ClassName {
    className(){}
}

class Player {
    private String name;
    private int level;

    Player(String name){
        this(name, 1);
    }

    Player(String name, int level){
        this.name = name;
        this.level = level;
    }
}
```
## Objektbeziehungen & Typbeziehungen

**Assoziation**: Ein Objekt kennte andere Objekte und kann Anforderungen weitergeben
Es gibt unidirektionale und bidirektionale Assoziationen. Die Multiplizitaet/Kardinalitaet gibt an mit wie vielen Objekten eine Seite eine Beziehung hat oder haben kann.

**Vererbung:** Ist-eine-Art-von Beziehung Generalisierung/Spezialisierung

**Assoziation:** hat-Beziehung Auto hat Reifen

Eltern geben Kindern ihre Eigenschaften mit. Bindet Klassen dicht aneinander. Macht Objekte austauschbar. Unterklassen erben von Oberklassen.

```java
public class GameObject {
    public String name;
}

public class Player extends GameObject {}
public class Room extends GameObject {
    public int size;
}
```

Alle Klassen haben ```Object``` als Basisklasse

Eine Unterklasse erbt alle sichtbaren Eigenschaften. Mit ```super()``` kann der Konstruktor und andere Methoden der Oberklasse aufgerufen werden. Muss immer als erstes stehen falls es sich um den Konstruktor handelt. Immer wenn ein Typ gefordert its, ist auch ein Untertyp erlaubt.

```java
Referenztyp name = new Objekttyp();
```
Compiler kennt nur den Referenztyp.

**Listkovsches Substitutionsprinzip:** Anstelle eines Objekts koennen auch Objekte einer Unterklasse eingesetzt werden.

### Typen mit ```instanceof``` testen

```java
"Toll" instanceof String // true
"Toll" instanceof Object // true
o.getClass == Object.class
```

### Ueberschreiben von Methoden

```java
@Override
public String toString(){
    return "Neue Rueckgabe bei Aufruf";
}
```

### Polymorphie (Dynamische Bindung)

Die Laufzeitumgebung nutzt den Objekttyp und nicht wie der Compiler den Referenztyp. Erst zur Laufzeit waehlt die Laufzeitumgebung die richtige Objektmethode aus, passend zum tatsaechlichen Typ des Objekts.

## Abstrakte Klassen und Methoden

- eine Unterklasse muss gebildet werden
- von Abstrakten Klassen koennen keine Objekte gebildet werden
- dient lediglich als Schritt in der Vererbungshierarchie
- git Signatur fuer Unterklassen vor
- abstrakte Methoden geben nur eine Signatur ohne Implementierung vor, Unterklassen muessen diese dann Implementieren


## Interfaces/Schnittstellen

- Trennen von was und wie
- Klassen implementieren Schnittstellen (```implements```)
- Wichtig, da es in Java nur Einfachvererbung gibt

### Schnittstellen koennen enthalten:

- abstrakte Methoden
- private und public konkrete Methoden (Default-Methoden)
- private und public statische Methoden
- Konstanten
- geschachtelte Typen

### Schnittestellen deklarieren

```java
interface Tradeable {}
interface Buyable {
    double price(); // public abstract by default
}

public class Chocolate implements Buyable {
    double price = 0.69;
    @Override
    public double price(){
        return price;
    }
}
```

Default Methoden in interfaces koenne mit ```this``` arbeiten und greifen ueber Polymorphismus auf die Methoden des Objekts zu, welches das Interface implementiert.