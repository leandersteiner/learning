# C++

### Ganze Zahlen:
- **short:** 16 Bits / 2 Byte
- **int:** 32 Bits / 4 Byte
- **long:** 32 oder 64 Bits / 4 oder 8 Byte
- **long long:** min. 64 Bits / min. 8 Byte
- **unsigned:** Zahlen können nicht negativ sein, Wertebereich n*2+1
- **Suffixe:**
  - **long, long long:** l/L , ll/LL
  - **unsigned:** u/U
- **Präfixe:**
  - **0b/0B:** Interpretation als Binärzahl (Bsp: 0b1101)
  - **0:** Interpretation als Oktalzahl (Bsp: 0377)
  - **0x/0X:** Interpretation als Hexadezimalzahl (Bsp: 0xAFFE)
  - Hochkommas zur besseren Lesbarkeit von Zahlen (Bsp: 1'000'000)
  - Deklarierung: Variable erstellen ohne einen Wert zuzuweisen
  - Initialisierung: Variable bereits bei der Deklarierung einen Wert zuweisen
    ```cpp
    int var; // Deklarierung, wir wissen den Inhalt nicht
    int var {1}; // Initialisierung
    int var = 1; // Initialisierung
    int var {}; // Initialisierung mit 0
    auto var {1}; // Initialisierung
    ```
  - **size_t:** Vorzeichenloser Ganzzahl-Typ für Größenangaben

---

### Reele Zahlen
- **float:** 32 Bits / 4 Byte
- **double:** 64 Bits / 8 Byte 

### Logischer Datentyp
- **bool:** - Kann nur true oder false beinhalten

---

### Konstanten

- Unveränderliche Variablen
    ```cpp
    const float PI = 3.1415926f;
    constexpr unsigned int ANZAHL = 1000;
    ```
- Konstantent werden komplett groß geschrieben

---

### Zeichen
```cpp
signed char character;
unsigned char character;
char character;
wchar_t character; // Wide char wenn 1 Byte nicht reicht

// Beispiele:
const char STERN {'*'}; // Chars in einfachen Anführungszeichen
char a;
a = 'a';

// Typumwandlung:
char c {'x'};
int i = static_cast<int>(c);

// Kurzformen unsicherer
int j = (int)c;
int k = int(c);
int l = c;
```

---

### Bit-Operatoren

Symbol|Verwendung|Funktion
:-:|:-:|:-:
<<|```i << 2```|Multiplizieren mit 2er-Potenz (Linksschieben)
\>>|```i >> 1```|Division durch 2er-Potenz (Rechtsschieben)
&|```i & 7```|Bitweises UND
^|```i ^ 7```|Bitweises XOR (Exklusives ODER)
\||```i \| 7```|Bitweises ODER
~|```~i```|Bitweise Negation

---

### Referenzen
- Liefert einen Verweis auf ein Objekt
- Es kann beliebig viele für ein Objekt geben
```cpp
int i {2};
int& r {i}; // r ist alias für i
r = 10; // ändert i: i=10
```

---

### Aufzählungstypen
```cpp
enum class Farben {GELB, BLAU, SCHWARZ};
Farben farbe = Farben::GELB;
int i = static_cast<int>(farbe);
std::cout << static_cast<int>(farbe) << std::endl;
```

---

### Strukturen(structs) / benutzerdefinierte Datentypen
Syntax: ```struct Typname {elemente} Variablenliste;```
```cpp
struct Person {
    std::string name;
    int alter;
} leander;
// Eigenschaften Initialisieren
leander.name = "Leander Steiner";
leander.alter = 22;
// Ausgeben der Eigenschaften
std::cout << leander.name << " " << leander.alter;
// Neue Instanz erstellen
Person Erik {"Erik Mueller", 21};
```

---

### Vektoren
Library einbinden: ```#include<vector>```
```cpp
std::vector<int> v{7, 0, 10}; // Vektor Initialisieren
std::cout << v.size(); // Gibt 3 aus
std::cout << v[2]; // Gibt 10 aus
v.push_back(12); // v[3] ist nun 12

// Kurzform zum auslesen aller Werte eines Vektors
for(int wert : v) {
    std::cout << wert << '\n';
}

// Wenn veränderung nötig ist muss eine Referenz übergeben werden
for(int& wert : v) {
    wert += 2;
}

// Gilt auch für Strings und andere Container
```
- ```push_back()``` fügt am Ende des Vektors einen neuen Wert an

---

### Strings
Library einbinden: ```#include<string>```
```cpp
std::string einString {"Hallo Welt"};
std::cout << einString[2]; // Gibt l aus
std::cout << einString.size() << einString.length(); // Gibt zwei mal 10 aus
```

---

### auto
Bei Variablen mit Typangabe ```auto``` entscheidet der Compiler selbst welcher Datentyp gebraucht wird.
```cpp
auto zahl = 4; // Zahl ist ein int
```
Dies funktioniert auch für selbsterstellte Datentypen oder Container wie ```vector```

---

### Dateneingabe
```cpp
std::string name;
getline(cin, name);
std::cout << name << '\n';

std::string nachname;
std::cin >> nachname;
std::cout << nachname << '\n';
```

---

### Schreiben und Lesen von Textdatein
- Library einbinde: ```#include<fstream>```
- **ofstream:** schreiben
    ```cpp
    ofstream datei("test.txt");
    if(!datei.good()){
        std::cerr << "Datei kann nicht beschrieben werden";
    }else{
        datei << "Geschriebener Text\nZeile 2\n;
        datei.close();
    }
    ```
- **ifstream:** lesen
    ```cpp
    ifstream datei("test.txt");
    while(datei.good()){
        std::string zeile;
        getline(datei, zeile);
        std::cout << zeile << '\n';
    }
    ```

---

### Funktionen
- Funktionsprototypen:
  - ```unsigned long funktion(unsigned int)```
  - Sagt dem Compiler das diese Funktion irgendwo definiert ist
- Funktionsdefinition:
  - ```Rückgabetyp Funktionsname(Parameter){Funktionsinhalt}```
  - ```unsigned long Funktion(unsigned int zahl){..}```
  - static Variablen verlieren ihren Wert zwischen Funktionsaufrufen nicht
- Parameterübergabe per Wert
  - Wert wird kopiert und in der Funktion dann neu definiert
  - Original bleibt unverändert
- Parameterübergabe per Referenz
  - Das Original wird übergeben und kann verändert werden
  - Laufzeitvorteil bei großen Objekten, da keine Kopie gemacht wird
  - Wenn nicht veränderbar sein soll als const übergeben
- Überladen von Funktionen:
  - Es können Funktionen mit dem gleichen Namen existieren, solange sie sich von den Parametern her unterscheiden