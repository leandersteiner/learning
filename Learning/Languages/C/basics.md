# Basics

## Bezeichner

- Namen von Objekten:
    - Variablen
    - Konstanten
    - Funktionen
- Darf bestehehen aus:
    - Buchstaben
    - Ziffern
    - Unterstriche
- Erstes Zeichen darf keine Ziffer sein
- case sensitive
-  Von C reservierte Schluesselwoerter duerfen nicht verwendet werden

## Literale
- Wahrheitswerte, Zeichenketten und Zahlen im Code
- Ganzzahlen
- Flieẞkommazahlen
- Character
- Zeichenketten

## Begrenzer
- ;
- ,
- {}
- =

## Kommentare

```c
// Einzeiliges Kommentar

/* 
Mehrzeiliges
Kommentar
*/
```

## Zeichensatz
- ASCII

## Steuerzeichen

```
\a - bell
\b - backspace
\f - Seitenvorschub
\n - newline
\r - carriage return
\t - horizontal tab
\v - vertical tab
\" - "
\' - '
\? - ?
\\ - \
\0 - Endmarkierung eines Strings
```

## Strings
- Array von chars

```c
char name[4] = "Bob"; //letztes Byte automatisch mit 0-Byte besetzt
printf("%s", name);
```