# Hello World

```c
#include <stdio.h>

int main(void){
    printf("Hello World\n");
    return 0;
}
```

## Einbinden von Bibliotheken/Dateien

```c
#include <stdio.h> // Praeprozessordirektive zum einbinden einer Bibliothek/Datei
```

## Einstiegspunkt eines Programs

```c
int main(void){ // Einstiegspunkt eines Programs
    // Anweisungen
} 
```

## Rueckgabe einer Funktion

```c
return 0; // Rueckgabewert einer Funktion
```

## Funktionsaufruf

```c
printf(""); // printf() Funktion aus stdio.h
```

## Kompilieren und starten des Programs

```
gcc helloworld.c -o helloworld
./helloworld(.exe)
```