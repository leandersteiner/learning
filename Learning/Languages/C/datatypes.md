# Datentypen

## Ganze Zahlen

```c
// short 2 Byte
short a;
a = 1;
short b = 2;
short c = 3, d = 4;
printf("Zahl1: %d - Zahl2: %d", a, b);

// int 2 - 4 Byte
int a;
a = 1;
int b = 2;
int c = 3, d = 4;
printf("Zahl1: %d - Zahl2: %d", a, b);

// long 4 Byte
long a;
a = 1;
long b = 2;
long c = 3, d = 4;
printf("Zahl1: %ld - Zahl2: %ld", a, b);

// long long 8 Byte
long long a;
a = 1;
long long b = 2;
long long c = 3, d = 4;
printf("Zahl1: %lld - Zahl2: %lld", a, b);
```

### Weitere Header

```c
#include <stdint.h> // Ganzzahlige Typen mit vorgegebener Breite
#include <inttypes.h>
```

## Gleitpunkt Zahlen

```c
// float 4 Byte
float a = 1.2;
printf("%f", a);

// double 8 Byte
double b = 3.65;
printf("%lf", a);

// long double 10 Byte
long double c = 1.234;
printf("%Lf", a);
```

## Character

```c
// char 1 Byte
char a = 'A';
char b = 65;
char c = b + 1;
printf("%c, %c, %c, %d", a, b, c, c);

// wchar_t 2 Byte
#include <stddef.h>
wchar_t ch1 = L'Z';
printf("%lc", ch1);
```

```c
#include <stdio.h>
#include <stddef.h>

int main(void)
{
    wchar_t ch1 = L'Z';
    wchar_t ch2;

    printf("Bitte ein Zeichen eingeben: ");
    scanf("%lc", &ch2);
    printf("%lc %lc\n", ch1, ch2);
    printf("wchar_t: %d Bytes\n", sizeof(wchar_t));
    return 0;
}

```

## Wahrheitswerte

```c
// Ohne Library oder typedef
_Bool test = 1;

if(test){
    printf("Test");
}

// Mit Library
#include <stdbool.h>
bool test = 1;

if(test){
    printf("Test");
}

// Mit typedef
typedef enum{false, true} bool;
bool test = 1;

if(test){
    printf("Test");
}
```