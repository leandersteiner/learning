# Basic HTML


## Basic Boilerplate
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
</body>
</html>
```

## Head
```html
<title>My Website</title>
<base href="https://domain.de/" target="_blank">
<link rel="stylesheet" href="style/main.css">
<style type="text/css">...</style>
<script type="text/javascript">...</script>
<meta name="author" content="Leander Steiner">
<meta name="keywords" content="key1, key2, key3">
<meta charset="UTF-8">
<meta name="robots" content="index,follow">
<meta name="description" content="...">
<link rel="icon" sizes="16x16" href="..."> <!-- Favicon -->
```

## Seitenstrukturierung
```html
<body></body> <!-- beinhaltet sichtbaren Teil einer Webseite -->
<section></section> <!-- Semantische unterteilung in Abschnitte -->
<article></article> <!-- in sich geschlossenes Element -->
<aside></aside> <!-- Sidebar / Nebeninfo -->
<nav></nav> <!-- Navigation -->
<header></header> <!-- Kopfberecih eines Inhalts -->
<footer></footer> <!-- Fußbereich eines INhalts -->
<address></address> <!-- Adressdaten -->
<div></div> <!-- Wie Section aber unsemantisch -->
<main></main> <!-- Hauptinhalt -->
```

## Textstrukturierung
```html
<p></p> <!-- Paragraph -->
<hr> <!-- Horizontal Rule -->
<blockquote></blockquote> <!-- Zitat -->
<h1></h1> - <h6></h6> <!-- Überschriften von groß (h1) bis klein (h6) -->
<figure></figure> <!-- Darstellung -->
<figcaption></figcaption> <!-- Beschriftung von Figure -->
<ul></ul> <!-- Unordered List -->
<ol></ol> <!-- Ordered List -->
<li></li> <!-- List Item -->
<dl></dl> <!-- Definition List -->
    <dt></dt>
    <dd></dd>
```

## Sonstige interessante Tags
```html
<abbr title="For You">4U</abbr> <!-- Abkürzung -->
<cite>Think and Grow Rich</cite> <!-- Literatur / Bibliografie -->
<code>Mein Code</code> <!-- Inline Code Angaben -->
<pre><code>...</code></pre> <!-- Mehrzeilige Code angaben -->
<span>...</span> <!-- Wie div nur inline -->
```

## Tabellen
```html
<table></table>
<tr></tr>
<td></td>
<th></th>
<thead></thead>
<tbody></tbody>
<tfoot></tfoot>
<colgroup></colgroup>
<col>
<caption></caption>
```

## Links
```html
<a href="link.html" target="_blank">Link Text</a>
<a href="#id">Link zu ID</a>
```

## Multimedia
```html
<img src="bild.jpg" alt="Bild beschreibung">
<map></map>
<area>
<picture></picture>
<source>
<svg></svg>
<math></math>
<canvas></canvas>
<video src="..." controls autoplay loop muted preload></video>
<audio src="..." controls autoplay loop muted preload></audio>
<embed>
<object></object>
<iframe></iframe>
```

## Formulare
```html
<form></form>
<fieldset></fieldset>
<legend></legend>
<label></label>
<datalist></datalist>
<input>
<button></button>
<select></select>
<option></option>
<textarea></textarea>
<output></output>
<progress></progress>
<meter></meter>
```
Attribute:
- \<form> : action, method
- \<input> : type, autofocus, autocomplete, list, max, min, multiple, pattern, placeholder, required, step, disabled, readonly, tabindex, accesskey
