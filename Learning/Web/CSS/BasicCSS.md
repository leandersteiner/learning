# Basic CSS

- HTML + Inhalt + CSS + Skripte + Media = Webseite
- CSS: Legt Regeln für die Darstellung der HTML-Elemente fest
- Selektoren: Bestimmen welches HTML-Element angesprochen wird
    ```css
    h1{ /* alle h1 Elemente werden angesprochen */ 
        color: white;   /* Zwischen den  stehen alle Darstellungsregeln */
    }
    ```
- Es gibt einfache und zusammengesetzte Selektoren
- **Einfache Selektoren:**
    ```css
    tag{}
    .klasse{}
    #id{}
    *{} /* Jedes Element */
    ```
- **Zusammengesetzte Selektoren:**
    ```css
    s1 s2{}
    s1>s2{}
    s1+s2{}
    s1~s2{}
    ```
- **CSS-Pseudoklasse:**
    ```css
    :link
    :visited
    :hover
    :focus
    :active
    :target
    :root
    :empty
    :first-child
    :last-child
    :nth-child(n)
    :nth-last-child(n)
    :only-child
    :first-of-type
    :last-of-type
    :nth-of-type(n)
    :nth-last-of-type(n)
    :only-of-type
    :not(p)
    :matches(.klasse)
    ```
- **CSS-Pseudoelemente:**
    ```css
    ::first-letter
    ::first-line
    ::before
    ::after
    ```
- **Größeneinheiten:**
    ```css
    .wrapper{
        width: 960px;
        width: 960pt;
        width: 960pc;
        width: 4em;
        width: 4rem;
        width: 80%;
        width: 100vw;
        height: 100vh;

        transform: rotate(90deg);
        transform: rotate(100grad);
        transform: rotate(5.5rad);
        transform: rotate(0.25turn);
    }
    ```
