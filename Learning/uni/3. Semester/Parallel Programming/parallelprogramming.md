# Einleitung

- Gleichzeitiger Zugriff auf eine gemeinsame Ressource
- Bsp: Drucker, Datenbank, usw.
- Prozesse & Threads
- Multiprocessing & Multithreading
- Greifen gleichzeitig auf Ressourcen zu
- Parallelität, Nebenläufigkeit, Concurrency
- Übliche Programme werden sequentiell abgearbeitet (durch einen Kern des Prozessors)
- Das Betriebssystem verwaltet dazu einen Thread innerhalb eines Prozesses
- Echte Parallelität ist auf einem Prozessor mit nur einem Kern nicht möglich -> Mehrere Kerne nötig

# Prozesse und Threads

- Programme erhalten beim start standarmäßig vom Betriebssystem mindestens einen Prozess(Task)
- Jeder Prozess hat ausführbaren Code, einen Adressraum zur alleinigen Nutzung
- Die Daten sind gegenüber anderen Prozessen abgeschottet -> Inter Process Communication (IPC)
- Ressourcen wie PID
- Ein Kern kann gleichzeitig nur einen Prozess bearbeiten
- Durch das Scheduling weist das Betriebssystem den PRozessen (Threads) einzelne Zeitscheiben zu. (Präemptives Multitasking)
- Sequentielle Programme benötigen pro Prozess nur einen Thread

## Thread: "Programmfaden"

- Mehrere Threads innerhalb eines Prozesses sind möglich
- Teilen sich Prozessressourcen z.B. Adressraum
- Weniger Overhead bei Scheduling
- Bemerkung: Parallele Programme sind oft schwierig zu debuggen

# Java

## Basisklasse "Thread"

```java
public class MyThread extends Thread {
    public void run() {
        System.out.println("Hello World");
    }
}

public static void main(String args[]) {
    MyThread t = new MyThread();
    t.start();
}
```

## Interface Runnable

```Java kann keine Mehrfachvererbung```

```java
public interface Runnable {
    public void run();
}
```

### Bsp:

```java
public class MyRun implements Runnable {
    public void run() {
        System.out.println("Hallo Welt");
    }
}

public static void main(String args[]) {
    MyRun r = new MyRun();
    Thread t = new Thread(r);
    t.start();
}
```

<hr>

## Wartezeiten

```java
public class Thread {
    public static void sleep(long ms) throws InterruptedException;
}
```

- ```sleep(long ms)``` Übergabewert in Millisekunden und ist ```static```
- legt aufrufenden Thread schlafen
- Kein "busy waiting", d.h. kein Rechenzeitverlust
- Das Betriebssystem nimmt den Thread die angegebene Zeit aus dem Scheduling
- Unvorhersagbare Ereignisse("spurious events") können den Thread vorzeitig aufwecken -> ```Exception``` wird geworfen
 
### Bem:
 
    Zeitliche Verzögerungen dürfen die Funktionsfähigkeit eines parallelen Programms nicht ändern!
 
<hr>
 
## Beenden von Threads
 
```Thread tot oder lebendig```
 
```java
public final boolean isAlive();
```
 
### Warten auf Beendigung eines Threads
 
1. "Busy waiting"
 
    ```java
    MyThread t = new MyThread();
    t.start();
    while(t.isAlive()){}
    // Jetzt ist Thread t tot
    ```
    Schlecht, da "busy waiting"
 
2. ```join()```
 
    ```java
    // wie vorher
    try{
      t.join(); // wartet auf das Ende von t
    }catch(InterruptedException e){
      // exception behandeln
    }
    ```
 
    ```java
    join(); // wartet unendlich lange
    join(long ms); // wartet höchstens eine Anzahl von Millisekunden
    join(long ms, int ns); // Mit Nanosekunden auch noch möglich
    ```

<hr>

## Unterbrechung von Threads

- Möglichkeit von "Software-interrupts" 
- Klasse Thread besitzt ein "Interrupt-Flag"

```java
public void interrupt(); // Setzt das Flag auf true
public boolean isInterrupted(); //  Abfrage des Flags
public static boolean interrupted(); // Flag-Abfrage im aktuellen Thread + Rücksetzen des Flags
```

### Ablauf:

- ```interrupt()``` setzt das flag des thread auf true.
- Wartet der Thread z.B. in ```sleep()``` oder ```join()```, wird das Warten unterbrochen. 
- Dabei wird durch eine Exception "```InterruptedException```" dieser Vorfall mitgeteilt
- Läuft der Thread, kann er sich über ```isInterrupted()``` oder ```interrupted()``` informieren, ob ein Interrupt vorliegt

<hr>

## Das Schlüsselwort "```volatile```"

- Compiler / Optimierer betrachten den Code nur "lokal"
- Es kann ihm entgehen, wenn der Wert einer Variablen von anderer Stelle verändert wird
- C und Abkömmlinge besitzen das Schlüsselwort ```volatile```
- Es weist den Compiler / Optimierer an, jederzeit mit Änderungen der Variable zu rechnen und deshalb allzu aggressive Oprimierungen zu unterlassen

### Typische Stellen:
- global sichtbare Variable wird über Thread-/Prozess-Grenzen hinweg verändert
- C/C++: Zugriff auf Hardware-Adressen

```java
private static volatile boolean flag = true;
```

<hr>

## Atomarität

### Allgemein: 
- Operation die entweder komplett fehlschlägt oder vollständig und unteilbar durchgeführt wird
- Bei gängigen CPUs / Betriebssystemen können das z.B. sein:
    - setzen einer "kleinen" Variablen
    - compare and swap: Speicherstelle mit einem Wert verleichen und evtl. setzen
    - fetch and add: Speicherzelle lesen und um eins erhöhen
- Nichtatomare Operationen in parallelen Programmen problematisch:

### <u>Beispiel:</u> Aufgabe 2 (bzw. 5)

### <u>Kritischer Abschnitt (critical section)</u>

- Erhoehung der Variable um 1 is nicht atomar -> Die Threads koennen sich in die Quere kommen.
- Prinzipieller Ablauf von ```val = val + 1```
- Wert von val wird aus RAM in Register geladen
- Register wird um 1 erhoeht
- Neuer Registerwert wird nach val zuruckgeschrieben

Nun kann folgendes passieren:

![Graph](pp1.png)

- D.h. beim Zurueckschreiben wird zweimal der um Eins erhoehte Wert nach val zurueckgeschrieben, obwohl zwei Erhoehungen stattgefunden haben.
- Der eine Thread gewinnt also den "Wettlauf" mit dem Anderen (race condition, data race).

```java
class MyThread extends Thread{
    private static final int N = 2000000;
    private int val = 0;

    public int value(){
        return val;
    }

    public void increment(){
        val++;
    }

    public void sum_up(){
        for(int n = 0; n < N; ++n){
            increment();
        }
    }

    public void run(){
        sum_up();
    }

    main(){
        MyThread my = new MyThread();
        my.start();
        sum_up();

        try{
            my.join()
        }catch(Exception e){

        }

        System.out.println(my.value());
    }
}
```

- <u>Zukuenftig also:</u> Mechanismen noetig, die einen Code-Abschnitt(critical section) vor gleichzeitigen Zugriffen schuetzt ("Lock")

- <u>Gegenteil:</u> Ein Code-Abschnitt heisst `thread-safe`, wenn er ohne Probleme von mehreren Threads gleichzeitig durchlaufen werden kann

- <u>Verwandter Begriff:</u> Eine Routine/Methode ist wiedereintrittsfaehig(reentrant), wenn sie von mehreren Threads/Prozessen gleichzeitig ohne gegenseitgie Beeinflussung durchlaufen werden kann.

<u>Bem:</u> Ressourcen/Variablen muessen auch geschuetzt werden, wenn man nur lesend darauf zugreift. Denn ein anderer Thread/Prozess koennte gerade schreibend und <u>nichtatomar</u> deren Wert veraendern. -> Inkonsistenz moeglich

<hr>

## Semaphore und Mutexe

In manchen Umgebungen sind evtl. nur einfach atomare Operationen und "busy-waiting" - Schleifen zur Synchronisation vorhanden

<u>Nachteil:</u>
- fehleranfaellig
- CPU-intensiv

<u>Besser:</u>
- Das Betriebssystem stellt grundlegende Mechanismen bereit
- Prozesse/Threads, die warten muessen, werden schlafen gelegt und erst bei Bedarf wieder geweckt

### Sehr verbreitet: <u>Semaphor (Dijkstra)</u> (zaehlend)

<br>

### <u>Prinzipieller Aufbau eines zaehlenden Semaphors:</u>

- Variable "```count```", auf Anfangswert N initialisiert
- Zwei atomare Zugriffsmethoden
- Methode ```p()/lock()/aquire()```  

    ```
    p(){
        while(count == 0); // wait
        --count;
    }
    ```

    Die ersten N Threads/Prozess, die ```p()``` aufrufen, werden durchgelassen. Alle spaeteren blockieren.

- Methode ```v()/unlock()/release()``` 

    ```
    v(){
        ++count;
        // wecke einen der wartenden Threads, falls vorhanden
    }
    ```

    Irgendein Thread verleasst die Methode

<u>Bem:</u>
- Nur maximal N Threads werden gleichzeitig in die zwischen ```p()``` und ```v()``` eingeschlossene ```critical section``` gelassen.
- Fuer "zu spaet Kommende" gibt es generell zwei Strategien:
    - warte und schlafe, bis dran
    - Bekomme mitgeteilt, dass man warten muesste, und mache deshalb was anderes
    - <u>Bem:</u> Bei uns hier immer "blockierend", d.h. Strategie 1
- Nicht unbedingt festgelegt, <u>welcher</u> wartende Thread durch ```v()``` aufgeweckt wird

<u>Besonders haeufig in Anwendungen:</u> binaere Semaphore (N = 1) fuer wechselseitigen Ausschluss

Oder spezieller: <u>Mutex</u> (mutual exclusion)

### Haeufigste Unterscheidung der beiden:
|bin. Semaphor|Mutex|
|-|-|
|geht auch prozessuebergreifend|threaduebergreifend, aber nur innerhalb eines Prozesses|
|```p()``` und ```v()``` koennen von unterschiedlichen Prozessen/Threads aufgerufen werden|muessen im gleichen Thread aufgerufen werden|
|Reihenfolge ```p()``` und ```v()``` "beliebig"| zuerst ```p()```, dann ```v()```|
|(Vorsicht bei mehrfachen/rekursiven aufrufen)|(Vorsicht bei mehrfachen/rekursiven aufrufen)|

<u>Beispiele:</u>

1. Aufgabe 2/5: "```++val```" ist kritischer Abschnitt => ganz typisch (fuer Mutexe insbesondere): 
    ```
    mutex.lock();
    ++val;
    mutex.unlock();
    ```

2. Vorrangfolge ("reader-writer-Problem")
    
    "Schreiber" muss Datei schreiben, bevor "Leser" lesen darf
    ```
    Semaphore sem = 1; // N = 1
    sem.p();
    // Erzeuge Leser und Schreiber (Threads)
    // Leser
        sem.p(); // blockiert!
    // Schreiber arbeitet
        write();
        sem.v(); // Leser wird aufgeweckt
    ```
    Entspricht also dem Szenario in Bsp_01
    Aber ohne deren Schwaechen

3. Erzeuger-Verbraucher-Problem (Producer-Consumer-Problem PCP)
    
    Lesende/Schreibende oder erzeugende/verbrauchende Zugriffe auf gemeinsame Ressourcen

    ![Graph](PCP.png)

    ```Ringpuffer, FIFO Liste```

    - Erzeuger/Verbraucher duerfen sich am gleichen Element nicht in die Quere kommen (Inkonsistenz)
    - Verbraucher will Element entnehmen, falls Keines vorhanden: warten
    - Erzeuger will Element erzeugen, muss warten, falls Puffer bereits gefuellt
