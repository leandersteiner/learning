# Von-Neumann'sches Rechnermodell

![alt](../../assets/von-neumann.png)

- Das Steuerwerk liest Befehle und Operanden nacheinander ein und interpretiert diese anhand einer Befehlstabelle
- Das Rechenwerk führt die entsprechenden arithmetischen und logischen Operationen durch
- Der Arbeitsspeicher enthält die Befehle und zugehörigen Daten laufender Programme
- Das Bussystem transportiert Daten zwischen den Einheiten
  
### Bussystem:

- Datenbus: Datenaustausch zwischen CPU und RAM
- Adressbus: Übertragung zugehöriger Speicheradressen
- Steuerbus: Koordiniert Zugriff auf Speicher und Peripherie mit Hilfe von Daten- und Adressbus

### Grundaufbau eines Prozessors:

Register sind Prozessorinterne Speicherplätze

- Arbeitsregister: 
  - Werden durch Namen angesprochen (z.B D1 - D7, EAX - EDX)
  - Können Daten oder Adressen aufnehmen
- Befehlszählerregister:
  - Beinhaltet immer die Adresse des als nächstes auszuführenden Befehls (IP: Instruction Pointer)
- Befehlsregister:
  - Kann einen (binären) Maschinenbefehl aufnehmen

Größe ist bei verschiedenen Prozessoren unterschiedlich.

Größe/Länge der **Arbeitsregister** legt die in einem Befehl größte verarbeitbare Zahl fest.
Bsp: 32-Bit Prozessor: 2^32 = 4.294.967.296
Bei größeren Zahlen: verarbeitung in mehreren Befehlen

Größe/Länge des **Befehlszählerregister** und **Adreregisters** legt die größe des adressierbaren Arbeitsspeichers fest.
32-Bit Prozessor: 2^32 4GB
