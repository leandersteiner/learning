# Befehlsformate

Opcode|1. Quelladresse|2. Quelladress|Zieladresse
:-:|:-:|:-:|:-:
OP|A|B|C

C = A OP B

Opcode|1. Quelladresse|2. Quelladress|Zieladresse
:-:|:-:|:-:|:-:
OP|A|A|B

A = A OP B

## Byteordnung im Datenaustausch
- little endian: vom rechten, niedrigerwertigen Ende beginnend
- big endian: vom linken, höherwertigen Ende beginnend

16 Bit Wort: 0x96C4

Position:|n|n+1|n|n+1
-|:-:|:-:|:-:|:-:
Bytes:|0xC4|0x96|0x96|0xC4
-|little|endian|big|endian

Standard Byteordnung im Internet: Big Endian